(function($) {
    "use strict";

    var allPanels = $('.accordion dd').hide();

    //mark roadmap dates
    $('.event-date').each(function (index) {
        $(this).parent().addClass(setUpcomingDate($(this).text())).addClass(setDirection(index));
    });

    //set current year for copyright
    $('#current_year').html((new Date()).getFullYear());

    // smooth scrolling using jQuery easing
    $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: (target.offset().top)
                }, calculateSpeed(target.offset().top), "easeOutCubic");
                return false;
            }
        }
    });

    function setUpcomingDate(dateLiteral) {
        return Date.parse(dateLiteral) > Date.now()?'upcoming':'';
    }

    function setDirection(index) {
        return index%2 === 0?'right':'left';
    }

    function calculateSpeed(targetScroll) {
        var speedLong = 5.5;
        var speedShort = 2;
        var diff = Math.abs(document.documentElement.scrollTop - targetScroll);
        return diff > 4000?diff/5.5:diff/speedShort;
    }

    //trigger overlay
    $('.overlay-trigger').click(function () {
        $('.overlay-trigger').not($(this)).parent().removeClass('open');
        $(this).parent().toggleClass('open');
    });

    //open accordion
    $('.accordion dt').click(function() {
        //allPanels.not($answer).slideUp();
        $(this).find('svg').toggleClass('open');
        $(this).next().slideToggle();
        return false;
    });

    //menu open
    $('#menu-icon').click(function () {
        $(this).closest('header').toggleClass('open');
    });

    //toogle on nav click
    $('header nav a').click(function () {
        var header = $(this).closest('header');
        if (header.hasClass('open'))
            header.toggleClass('open');
    });

    //init slider for tablet and mobile devices
    if ($(window).width() <= 1024) {
        $('#news-quotes').slick({
            infinite: true,
            centerMode: true,
            arrows: false,
            centerPadding: '10%',
            responsive: [
                {
                    breakpoint: 569,
                    settings: {
                        slidesToShow: 1
                    }
                },
                {
                    breakpoint: 769,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 1025,
                    settings: {
                        slidesToShow: 3
                    }
                }
            ]
        });
    }

})(jQuery);
